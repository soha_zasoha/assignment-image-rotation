#ifndef SOLUTION_ERROR_ALERT_H
#define SOLUTION_ERROR_ALERT_H

#include "bmp_status.h"

const char* read_alert(enum input_file_status read_status);

const char* write_alert(enum output_file_status write_status);

#endif
