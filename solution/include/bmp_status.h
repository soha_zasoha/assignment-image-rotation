#ifndef SOLUTION_BMP_STATUS_H
#define SOLUTION_BMP_STATUS_H

#include "image.h"

#define TYPE 0x4D42
#define SIZE 40
#define BIT_COUNT 24

enum input_file_status {
    READ_OK = 0,
    READ_INVALID_TYPE,
    READ_EMPTY_DATA,
    READ_PIXEL_ERROR,
    READ_PADDING_ERROR,
    READ_CLOSING_ERROR
};

enum output_file_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum input_file_status read_file(const char* file_name, struct image *image);

enum output_file_status write_file(const char* file_name, struct image const *image);

#endif
