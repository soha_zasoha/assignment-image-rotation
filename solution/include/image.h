#ifndef SOLUTION_IMAGE_H
#define SOLUTION_IMAGE_H

#include <stdint.h>


#pragma pack(push, 1)
struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};
#pragma pack(pop)

struct image {
    uint32_t width, height;
    struct pixel *data;
};

struct image rotation(struct image const *image);

uint8_t padding(struct image const *image);

struct image create_image(uint32_t height, uint32_t width);

void destroy_image(struct image *image);

#endif
