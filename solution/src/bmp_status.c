#include <stdint.h>
#include <stdio.h>

#include "bmp_status.h"

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

static struct bmp_header create_new_header(struct image const *image) {
    return(struct bmp_header){
            .bfType =  TYPE,
            .bfileSize = sizeof(struct bmp_header) + padding(image) *  image -> height,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = SIZE,
            .biWidth = image -> width,
            .biHeight = image -> height,
            .biPlanes = 1,
            .biBitCount = BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = image -> height * (image -> width + padding(image)),
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

enum input_file_status check_status(FILE *input_file, struct image *image) {
    uint32_t pix_count = 0;
    struct bmp_header header = {0};
    fread(&header, sizeof(struct bmp_header), 1, input_file);
    if (header.bfType != TYPE) {
        return READ_INVALID_TYPE;
    }
    *image = create_image(header.biHeight, header.biWidth);
    const uint8_t bmp_padding = padding(image);
    for (size_t i = 0; i < image -> height; i = i + 1) {
        pix_count = fread(&(image -> data[image -> width * i]), sizeof(struct pixel), image -> width, input_file);
        if (pix_count != image -> width) {
            destroy_image(image);
            return READ_PIXEL_ERROR;
        }
        if (fseek(input_file, bmp_padding, SEEK_CUR) != 0) {
            destroy_image(image);
            return READ_PADDING_ERROR;
        }
    }
    return READ_OK;
}

enum output_file_status check_write_status(FILE *output_file, struct image const *image) {
    struct bmp_header header = create_new_header(image);
    if (fwrite(&header, sizeof(struct bmp_header), 1, output_file) != 1) {
        return WRITE_ERROR;
    }
    const uint32_t waste = 0;
    const uint8_t bmp_padding = padding(image);
    for (size_t i = 0; i < image -> height; i = i + 1) {
        if (!fwrite(&image -> data[i * image -> width], sizeof(struct pixel),
                    image -> width, output_file) ||
            !fwrite(&waste, 1, bmp_padding, output_file)) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum input_file_status read_file(const char* file_name, struct image *image) {
    FILE *input_file;
    input_file = fopen(file_name, "rb");
    if (input_file == NULL) {
        fclose(input_file);
        return READ_EMPTY_DATA;
    }
    enum input_file_status read_status;
    read_status = check_status(input_file, image);
    if (fclose(input_file)) {
        return READ_CLOSING_ERROR;
    }
    return read_status;
}

enum output_file_status write_file(const char* file_name, struct image const *image) {
    FILE *output_file;
    output_file = fopen(file_name, "wb");
    enum output_file_status write_status;
    write_status = check_write_status(output_file, image);
    if (fclose(output_file)) {
        return WRITE_ERROR;
    }
    return write_status;
}
