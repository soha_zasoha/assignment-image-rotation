#include "error_alert.h"

const char* read_alerts[] = {
        [READ_INVALID_TYPE] = "\nInvalid type of file",
        [READ_EMPTY_DATA] = "\nYou didn't insert any files",
        [READ_PIXEL_ERROR] = "\n Number of pixels is not correct",
        [READ_PADDING_ERROR] = "\n Width of the picture is not correct",
        [READ_CLOSING_ERROR] = "\n File wasn't closed"
};

const char* write_alerts[] = {
        [WRITE_ERROR] = "Something went wrong",
        [WRITE_OK] = "Successfully done!"
};

const char* read_alert(enum input_file_status read_status) {
    return read_alerts[read_status];
}

const char* write_alert(enum output_file_status write_status) {
    return write_alerts[write_status];
}
