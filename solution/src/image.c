#include <stdlib.h>

#include "image.h"

static struct pixel pixel_set (struct image const* img, const size_t i, const size_t j) {
  return img -> data[i * img -> width + j];
}

struct image create_image(uint32_t height, uint32_t width) {
    return (struct image) {.height = height, .width = width, .data = malloc(sizeof(struct pixel) * height * width)};
}

void destroy_image(struct image *image) {
    if (image) {
        free(image -> data);
    }
}

struct image rotation(struct image const *image) {
    struct image image_res = create_image(image -> width, image -> height);
    for (size_t i = 0; i < image -> height; i = i + 1) {
        for (size_t j = 0; j < image -> width; j = j + 1) {
            image_res.data[(image -> height - 1 - i) + j * image -> height] = pixel_set(image, i, j);
        }
    }
    return image_res;
}

uint8_t padding(struct image const *image) {
    return image -> width % 4;
}
