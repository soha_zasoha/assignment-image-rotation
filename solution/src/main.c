#include <stdio.h>

#include "bmp_status.h"
#include "error_alert.h"

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Number of arguments is not correct");
    }
    struct image insert_image = {0};
    enum input_file_status read_status;
    read_status = read_file(argv[1], &insert_image);
    if (read_status != READ_OK) {
        fprintf(stderr, "%s\n", read_alert(read_status));
        return 1;
    }
    struct image image_res;
    image_res = rotation(&insert_image);
    destroy_image(&insert_image);
    enum output_file_status write_status;
    write_status = write_file(argv[2], &image_res);
    if (write_status == WRITE_OK) {
        fprintf(stderr, "%s\n", write_alert(write_status));
    }
    else {
        fprintf(stderr, "%s\n", write_alert(write_status));
        return 2;
    }
    destroy_image(&image_res);
    return 0;
}
